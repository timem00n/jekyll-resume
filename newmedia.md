<script src="https://unpkg.com/mermaid@7.1.2/dist/mermaid.min.js"></script> 
 
  <script>mermaid.initialize({startOnLoad:true});</script> 
 
  <div class="mermaid"> 
 graph LR; 
 
  Tech&gt;网络新媒体技术与协作] --&gt; Web 
 Tech --&gt; Information_Visualization 
 Tech --&gt; Python 
 Web --&gt; |技术栈线|Bootstrap[前端开发框架] 
 Bootstrap --&gt; |技术栈线|NodeJS[网新交互技术栈基础架] 
 NodeJS --&gt; |技术栈线|Vue[APP设计与制作] 
 Web --&gt; |交互界面|Interactions[互动技术与应用] 
 Interactions --&gt; |交互界面|Vue 
 Information_Visualization&gt;信息可视化设计]--&gt; |建立分析基础|Big_Data 
 Information_Visualization --&gt; |建立分析基础|UX 
 Information_Visualization --&gt; |应用设计|Interactions 
 Big_Data[大数据] --&gt; |应用设计| API_ML_AI[API机器学习与人工智能] 
 Big_Data --&gt;|应用设计| Big_Data_Capacity[大数据能力建设] 
 Big_Data --&gt; |应用设计|Platform[平台经济与创新] 
 Python&gt;Python语言] --&gt; |建立分析基础|Big_Data 
 Python --&gt; |技术栈线|NodeJS[网新交互技术栈基础架] 
 Web&gt;网页设计与制作] --&gt; |建立分析基础|UX[新媒体分析和用户体验] 
 Web --&gt; |建立敍事基础|Convergence[媒介融合] 
 Web --&gt; |建立敍事基础|Video_Editing[数字音视频编辑] 
 Convergence --&gt; |应用设计|Video_Editing 
 Web --&gt; |建立分析基础|Online_Survey[线上调查与统计] 
 Online_Survey --&gt; |建立分析基础|User_Research[用户研究] 
 Convergence --&gt; Platform 
 Python --&gt; |建立基础|API_ML_AI 
 UX --&gt; |应用设计|API_ML_AI 
 UX --&gt; |应用设计|User_Research 
 UX --&gt; |应用设计|Platform 
 Platform --&gt; |应用设计|Vue 
 User_Research --&gt; |应用设计|Vue 
 API_ML_AI --&gt; |应用设计|Vue 
 Big_Data_Capacity --&gt; |应用设计|Vue 
<script src="https://unpkg.com/mermaid@7.1.2/dist/mermaid.min.js"></script> 
 
  <script>mermaid.initialize({startOnLoad:true});</script> 
 
  <div class="mermaid"> 
 graph LR; 
 
  Tech&gt;网络新媒体技术与协作] --&gt; Web 
 Tech --&gt; Information_Visualization 
 Tech --&gt; Python 
 Web --&gt; |技术栈线|Bootstrap[前端开发框架] 
 Bootstrap --&gt; |技术栈线|NodeJS[网新交互技术栈基础架] 
 NodeJS --&gt; |技术栈线|Vue[APP设计与制作] 
 Web --&gt; |交互界面|Interactions[互动技术与应用] 
 Interactions --&gt; |交互界面|Vue 
 Information_Visualization&gt;信息可视化设计]--&gt; |建立分析基础|Big_Data 
 Information_Visualization --&gt; |建立分析基础|UX 
 Information_Visualization --&gt; |应用设计|Interactions 
 Big_D